import tornado.ioloop
import tornado.web
import tornado.autoreload
import time

import time
from datetime import datetime
import math
import threading
from envsensor import fileLogger

import json
import os
dest_dir='/data'
xpol_data_source='/users/radarop/xpold'

def intervalSleep(interval):
    now=time.time()
    time.sleep(int(now/interval+1)*interval-now)

    
        
def piedestalControl_loop():
    webcamLog=fileLogger.fileLogger('/users/radarop/log/','webcam')
    try:
        from envsensor import xpol
        ped=xpol.ped()
    except:
        print("cannot open pedestal for image logging")

    from downloadImage import downloadImage
    while True:
        now=datetime.utcnow()
        nowStr=datetime.strftime(now,'%Y%m%d-%H%M%S')
        
        try:
            try:
                pedData=ped.measure()
                imageLabel=nowStr+'az:%6.2f el:%6.2f'%(pedData['az'],pedData['el'])
            except:
                pedData={}
                imageLabel=''
            image_dest = dest_dir+'/webcam/%04d/%02d/%02d/%02d/'%(now.year,now.month,now.day,now.hour)
            if not os.path.isdir(image_dest):
                os.makedirs(image_dest)
            imageFullName=image_dest+nowStr+'.jpg'
            downloadImage('http://192.168.1.103/jpg/image.jpg',imageFullName,imageLabel)
            #print('here '+str(logData))
            logData=pedData.copy()
            logData['name']=imageFullName
            #print(logData)
            webcamLog.insert(logData)
        except Exception as e:
            webcamLog.insert({'error':'cannot get image "%s"'%str(e)})
        intervalSleep(5)
        
def systemCheck_loop():
    systemLog=fileLogger.fileLogger('/users/radarop/lteradar1/log/','SystemCheck')

    from envsensor import psData,loopsensors,xpol
    pcTemp=psData.Temperature()
    diskMonitor=psData.DiskUsage('/',name='root')
    diskMonitorUsers=psData.DiskUsage('/users',name='users')
    dataMonitor1a=psData.DiskUsage('/data1a/',name='data1a')
    dataMonitor1b=psData.DiskUsage('/data1b/',name='data1b')
    xpold=psData.Process('xpold')
    rcb=xpol.rcb()
    
    instrs=loopsensors.loopsensors(threaded=True)

    instrs.addSensor(pcTemp)
    instrs.addSensor(diskMonitor)
    instrs.addSensor(diskMonitorUsers)
    instrs.addSensor(dataMonitor1a)
    instrs.addSensor(dataMonitor1b)
    instrs.addSensor(xpold)
    instrs.addSensor(rcb)

    while True:
        
        try:
            systemLog.insert(instrs.measure())
        except Exception as e:
            systemLog.insert({'error':str(e)})
        intervalSleep(60)
        
"""def moveXpolData_loop():  
    from moveXpolData import moveXpolData 
    
    while True:
        try:
            moveXpolData(xpol_data_source,dest_dir)
        except:
            pass
        intervalSleep(60) """  

def checkRadarRunning_loop():
    systemLog=fileLogger.fileLogger('/users/radarop/log/','CheckRadarRunning')

    while True:
        systemLog.insert({'message':'test'})
        intervalSleep(10)
            
        

#piedestal_thread = threading.Thread(target=piedestalControl_loop)
systemCheck_thread = threading.Thread(target=systemCheck_loop)
#moveXpolData_thread = threading.Thread(target=moveXpolData_loop)
#checkRadarRunning_thread = threading.Thread(target=checkRadarRunning_loop)

#piedestal_thread.start()
systemCheck_thread.start()
#moveXpolData_thread.start()
#checkRadarRunning_thread.start()

tornado.web.Application()
tornado.autoreload.start(check_time=1000)
tornado.ioloop.IOLoop.instance().start()

