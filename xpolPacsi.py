import os
import subprocess
import re
import shutil
import psutil
import glob
import sys
import time
from envsensor import fileLogger

import tornado.ioloop
import tornado.web
import tornado.autoreload
import threading
from envsensor import fileLogger
import json

source_dir = '/users/radarop/lteradar1/xpold'
time_lag = 180
size_thres = 300e6
pax_file= '/users/radarop/pax_files/PAPET_scan.pax'
log_pax=fileLogger.fileLogger('/users/radarop/lteradar1/log/','Pacsi')

def intervalSleep(interval):
    now=time.time()
    time.sleep(int(now/interval+1)*interval-now)

def checkDataCreation(source_dir,time_lag): #time_lag must be in seconds
    sorteddat = glob.glob(source_dir+'/*/*.dat')
    sorteddat.sort(key=os.path.getmtime)
    latesttime = os.path.getmtime(sorteddat[-1])
    if time.time()-latesttime<time_lag:
        return True
    else:
        return False

def checkFileTooLarge(source_dir,size_thres):
    sorteddat = glob.glob(source_dir+'/*/*.dat')
    sorteddat.sort(key=os.path.getmtime)
    if os.path.getsize(sorteddat[-1])>size_thres:
        return True
    else:
        return False
    
def restartPacsi(pax_file):
    launch_pacsi = subprocess.run(["./launchPacsi",pax_file],stdout=subprocess.PIPE)
    try:
        log_pax.insert({'launch pacsi':launch_pacsi.stdout.decode()})
    except Exception as e:
        log_pax.insert({'launch pacsi error':e.message})
        pass
    
def Pacsi_loop():
    while True:
        try:
            if not checkDataCreation(source_dir,time_lag):
                restartPacsi(pax_file)
            if checkFileTooLarge(source_dir,size_thres):
                killxpold = subprocess.run(["killall","xpold"],stdout=subprocess.PIPE)
                log_pax.insert({'kill xpold':killxpold.stdout.decode()})
                time.sleep(10)
                restartPacsi(pax_file)
        except Exception as e:
            print("error:"+e.message)
            pass 
        intervalSleep(60)

xpolPacsi_thread = threading.Thread(target=Pacsi_loop)

#quicklooks_thread.start()
xpolPacsi_thread.start()

tornado.web.Application()
tornado.autoreload.start(check_time=1000)
tornado.ioloop.IOLoop.instance().start()