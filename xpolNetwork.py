import os
import subprocess
import re
import shutil
import glob
import sys
import time

#import tornado.ioloop
#import tornado.web
#import tornado.autoreload
import threading
import json

#source_dir = '/users/radarop/lteradar1/xpold'
time_lag = 180
#size_thres = 300e6
#pax_file= '/users/radarop/pax_files/PAPET_scan.pax'
#log_pax=fileLogger.fileLogger('/users/radarop/lteradar1/log/','Pacsi')

def intervalSleep(interval):
    now=time.time()
    time.sleep(int(now/interval+1)*interval-now)

def checkPing():
    ping = subprocess.run(["ping","-c","1","www.epfl.ch"],stderr=subprocess.PIPE,stdout=subprocess.PIPE)
    err = ping.stderr.decode()
    out = ping.stdout.decode()
    
    if (len(err) > 0) and (len(out)==0):
        return False
    else:
        return True
        
           
def Network_loop():
    while True:
        try:
            if not checkPing():
                print('restarting network')
                subprocess.run(["systemctl","restart","networking"],stdout=subprocess.PIPE)
                print('restarted'+stdout.decode())
            
        except Exception as e:
            print("error:"+e.message)
            pass 
        intervalSleep(time_lag)

xpolNetwork_thread = threading.Thread(target=Network_loop)

xpolNetwork_thread.start()

#quicklooks_thread.start()
while True:
    time.sleep(10)
    """
tornado.web.Application()
tornado.autoreload.start(check_time=1000)
tornado.ioloop.IOLoop.instance().start()"""