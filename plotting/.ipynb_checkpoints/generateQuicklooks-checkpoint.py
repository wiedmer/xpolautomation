#!/usr/bin/env python

import glob
import sys
import os
import time
from pyjacopo import parse_config, read_raw_data
from plotting.plot_quicklooks import plot_RHI,  plot_sector
from envsensor import fileLogger
from datetime import datetime


def logHeaderData(records):
    #variableLogger=logger.fileLogger('/tmp/lteradar1/log/','xpold_scan_variables')
    variableLogger=fileLogger.fileLogger('/users/radarop/lteradar1/log/','xpold_scan_variables')
    
    tempVariableList=['temp (RF)','temp (plate)','temp (pod air)','temp (PC cover)']
    copyVariableList= ['tx_pow_sam','az','el','az_vel','el_vel']
    date=datetime.fromtimestamp(0)
    
    for scanType in records:
        headers=records[scanType]['header']
        for header in headers:
            try:
                date=datetime.fromtimestamp(header['host_ts'][0]+header['host_ts'][1]*1e-6)
                data={}
                for tempVar in tempVariableList:
                    data[tempVar]=(header[tempVar]-282)/6.214

                for copyVar in copyVariableList:
                    data[copyVar]=header[copyVar]
                variableLogger.insert(data,date)
            except Exception as e:
                print("error "+raw_filename)
                pass

def generateQuicklooks(data_dir,figures_dir):
    #sort data files by modification date
    files = sorted(glob.glob(data_dir+'/*/*.dat'))
    #files.sort(key=os.path.getmtime)

    #sort quicklooks by modification date
    figures = glob.glob(figures_dir+'/*/*.png')
    if len(figures)>0:
        #print(figures)
        figures.sort(key=os.path.getmtime)
        latest_fig_time = os.path.getmtime(figures[-1])
    else:
        latest_fig_time = -1
    #create quicklooks for all files that were written since the latest quicklook
    fcounter = 1
    f = files[-fcounter]
    while (os.path.getmtime(f) > latest_fig_time) and (fcounter < len(files)):
        #print(f)
        name = f.split('/')[-1][:-4]
        date = f.split('/')[-2]
        #print(figures_dir+'/'+name)
        if not os.path.exists(figures_dir+'/'+date):
            os.mkdir(figures_dir+'/'+date)
            
        try:
            config = parse_config('plotting/config.yml')
            header, records = read_raw_data(f, config)
            
            try:
                logHeaderData(records)
            except Exception as e:
                print("Could not get data from %s exception %s"%(f,str(e)))
                pass
            figureName=figures_dir+'/'+date+'/'+name
            try:
                if 'RHI' in records:
                    plot_RHI(header,records,config,figureName)
                if 'Point' in records:
                    plot_sector(header,records,config,figureName)
            except Exception as e:
                print("Could generate quicklook for %s exception %s"%(f,str(e)))
        except Exception as e:
            print("Could not get data for %s exception %s"%(f,str(e)))
        fcounter+=1
        f = files[-fcounter]
        
        
if __name__ == "__main__":
    #data_dir = sys.argv[1]
    #figures_dir = sys.argv[2]
    while True:
        data_dir="/users/radarop/lteradar1/xpold"
        figures_dir="/users/radarop/lteradar1/quicklooks"
        generateQuicklooks(data_dir,figures_dir)
        time.sleep(60)
