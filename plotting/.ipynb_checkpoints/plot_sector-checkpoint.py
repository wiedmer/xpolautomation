from pyjacopo import parse_config, read_raw_data
from pyjacopo import write_cfradial, read_cfradial
from pyjacopo import process_dataset
import glob
import pyart
import matplotlib.pyplot as plt
import sys
import numpy as np
import os
import datetime

elev_low = 5
elev_high = 8

def plot_sector(raw_filename,outname):
    config = parse_config('config.yml')
    header, records = read_raw_data(raw_filename, config)
    pyart_instance = process_dataset('SECTOR_SCAN',header,records['Point'],config)
    
       
    name = raw_filename.split('/')[-1][:-4]   
    el = round(pyart_instance.elevation['data'][-20:].mean())
    
    if (abs(el-elev_low) <= 1 or abs(el-elev_high) <=1):
        if abs(el-elev_low) < abs(el-elev_high):
            elev_ref = elev_low
        else:
            elev_ref = elev_high
        
        dt = datetime.datetime.utcfromtimestamp(pyart_instance.time['data'][0])
        dtstr = dt.strftime('%Y-%m-%dT%H:%M:%SZ') 
        xlim = [-27,0]
        ylim = [-27,27]
        
        x = np.array(pyart_instance.gate_x['data'])/1000
        y = np.array(pyart_instance.gate_y['data'])/1000
        pyart_instance.add_field('-Zdr',pyart_instance.fields['Zdr'])
        pyart_instance.fields['-Zdr']['data'].data[:] = -pyart_instance.fields['Zdr']['data'].data[:]
        elevations = pyart_instance.elevation['data']
        var = 'Zh'
        Var = pyart_instance.fields[var]['data']
        
        fig = plt.figure(figsize=[9, 3])
        Var[abs(elevations-elev_ref)>.2,:] = np.nan
        
        ax = fig.add_subplot(131)
        ax.set_aspect('equal')
        im=ax.pcolormesh(x,y,Var,cmap='jet',vmin=0,vmax=55)
        ax.set_xlim(xlim)
        ax.set_ylim(ylim)
        ax.set_title('Reflectivity')
        ax.set_xlabel('Range x (km)',fontsize=10)
        ax.set_ylabel('Range y (km)',fontsize=10)
        ax.yaxis.set_label_coords(-0.3, 0.5)
        cbar=fig.colorbar(im,ax=ax)
        cbar.set_label('Reflectivity (dBZ)',fontsize=10)
        cbar.ax.tick_params(labelsize=8)
        
        var = '-Zdr'
        Var = pyart_instance.fields[var]['data']
        Var[abs(elevations-elev_ref)>.2,:] = np.nan
        ax = fig.add_subplot(132)
        ax.set_aspect('equal')
        im=ax.pcolormesh(x,y,Var,cmap='jet',vmin=-1,vmax=3)
        ax.set_xlim(xlim)
        ax.set_ylim(ylim)
        ax.set_title('Diff. reflectivity')
        ax.set_xlabel('Range x (km)',fontsize=10)
        ax.set_ylabel('Range y (km)',fontsize=10)
        ax.yaxis.set_label_coords(-0.3, 0.5)
        cbar = fig.colorbar(im,ax=ax)
        cbar.set_label('Diff. reflectivity (dBZ)',fontsize=10)
        cbar.ax.tick_params(labelsize=8)
        
        var = 'RVel'
        Var = pyart_instance.fields[var]['data']
        Var[abs(elevations-elev_ref)>.2,:] = np.nan
        ax = fig.add_subplot(133)
        ax.set_aspect('equal')
        im=ax.pcolormesh(x,y,Var,cmap='bwr',vmin=-40,vmax=40)
        ax.set_xlim(xlim)
        ax.set_ylim(ylim)
        ax.set_title('Mean Doppler velocity')
        ax.set_xlabel('Range x (km)',fontsize=10)
        ax.set_ylabel('Range y (km)',fontsize=10)
        ax.yaxis.set_label_coords(-0.3, 0.5)
        cbar = fig.colorbar(im,ax=ax)
        cbar.set_label('Mean Doppler velocity (m/s)',fontsize=10)
        cbar.ax.tick_params(labelsize=8)
        
        fig.suptitle('Elevation %d - '%el + dtstr,x=.5,y=1.05,fontsize=14,fontweight='bold')
        
        fig.savefig(outname+'_el'+str(elev_ref),bbox_inches='tight')
        plt.close()

                
                ##
"""if __name__ == '__main__':
    raw_filename = 'XPOL-20200204-002302.dat'
    outname = 'test_sector5.png'
    plot_sector(raw_filename,outname)"""