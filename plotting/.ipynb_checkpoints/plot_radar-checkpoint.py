from pyjacopo import parse_config, read_raw_data
from pyjacopo import write_cfradial, read_cfradial
from pyjacopo import process_dataset
import glob
import pyart
import matplotlib.pyplot as plt
import sys

config = parse_config('config.yml')

raw_filename = sys.argv[1]


header, records = read_raw_data(raw_filename, config)

try:
    pyart_instance = process_dataset('RHI',header,records['RHI'],config)
    display = pyart.graph.RadarDisplay(pyart_instance)
    
    fig = plt.figure(figsize=[7, 5])
    ax = fig.add_subplot(311)
    var = 'Zh'
    display.plot(var, cmap='jet')
    
    ax = fig.add_subplot(312)
    var = 'Zdr'
    display.plot(var,cmap='jet')
    
    ax = fig.add_subplot(313)
    var = 'Rvel'
    display.plot(var,cmap='jet')
    
    fig.savefig('RHI_test')
    
except:
    pass

