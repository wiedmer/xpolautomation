from pyjacopo import parse_config, read_raw_data
from pyjacopo import write_cfradial, read_cfradial
from pyjacopo import process_dataset
import glob
import pyart
import matplotlib.pyplot as plt
import sys
import numpy as np
az_suchet = 311.8
az_clees = 279.8
az_mont_tendre = 235.8


"""def plot_RHI(raw_filename,outname):
    config = parse_config('/users/radarop/xpolAutomation/plotting/config.yml')
    try:
        header, records = read_raw_data(raw_filename, config)
        pyart_instance = process_dataset('RHI',header,records['RHI'],config)
        display = pyart.graph.RadarDisplay(pyart_instance)

        fig = plt.figure(figsize=[15, 3])
        ax = fig.add_subplot(131)
        var = 'Zh'
        display.plot(var, cmap='jet')
        display.set_limits(xlim=[0,5],ylim=[0,5])

        ax = fig.add_subplot(132)
        var = 'Zdr'
        display.plot(var,cmap='jet')
        display.set_limits(xlim=[0,5],ylim=[0,5])

        ax = fig.add_subplot(133)
        var = 'RVel'
        display.plot(var,cmap='bwr')
        display.set_limits(xlim=[0,5],ylim=[0,5])

        fig.savefig(outname,bbox_inches='tight')
        plt.close()
    
    except:
        print('no plot')
        pass"""

    
def plot_RHI(raw_filename,outname):
    config = parse_config('config.yml')
#    for raw_filename in raw_filenames:
        
    #def plot_RHI(raw_filename,outname)
  #  try:
    
    xlim = [-15,15]
    ar = 2
    ytitle = 1.05
    vn = 40
    header, records = read_raw_data(raw_filename, config)
    pyart_instance = process_dataset('RHI',header,records['RHI'],config)
    display = pyart.graph.RadarDisplay(pyart_instance)
    name = raw_filename.split('/')[-1][:-4]   
    az = pyart_instance.azimuth['data'].data[0]
    azname = str(round(az))
    if abs(az-az_suchet) < 2:
        target = 'Suchet'
        azname = str(int(az_suchet))
        xlim = [0,15]
        ar = 1.2
        ytitle = 1.1
        vn = 12

    elif abs(az-az_clees) < 2:
        target = 'Les Clees'
        azname = str(int(az_clees))
        xlim = [-15,15]
        ar = 2
        ytitle = 1.05
        vn = 40

    elif abs(az-az_mont_tendre) < 2:
        target = 'Mont Tendre'
        azname = str(int(az_mont_tendre))
        xlim=[0,15]
        ar = 1.2
        ytitle = 1.1
        vn = 12

        
        
    fig = plt.figure(figsize=[15, 3])
    ax = fig.add_subplot(131)
    var = 'Zh'
    display.plot(var, cmap='jet')
    display.set_limits(xlim=xlim,ylim=[0,10])
    display.set_aspect_ratio(ar)
        
    ax = fig.add_subplot(132)
    var = '-Zdr'
    pyart_instance.add_field('-Zdr',pyart_instance.fields['Zdr'])
    pyart_instance.fields['-Zdr']['data'].data[:] = -pyart_instance.fields['Zdr']['data'].data[:]
    display.plot(var,cmap='jet',vmin=-1,vmax=3)
    display.set_limits(xlim=xlim,ylim=[0,10])
    display.set_aspect_ratio(ar)
        
    ax = fig.add_subplot(133)
    var = 'RVel'
    display.plot(var,cmap='bwr',vmin=-vn,vmax=vn)
    display.set_aspect_ratio(ar)
    display.set_limits(xlim=xlim,ylim=[0,10])
        
    fig.suptitle(target,x=.5,y=ytitle,fontsize=14,fontweight='bold')
    fig.savefig(outname+'_az'+azname,bbox_inches='tight')#,dpi=200)
    plt.close()
    
   # except:
   #     print('no plot')
   #     pass

"""if __name__ == '__main__':
	raw_filename = 'XPOL-20200204-002123.dat'
	outname = 'test_RHI'
	plot_RHI(raw_filename,outname)"""


