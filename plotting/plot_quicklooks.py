
from pyjacopo import parse_config, read_raw_data
from pyjacopo import write_cfradial, read_cfradial
from pyjacopo import process_dataset
import glob
import pyart
import matplotlib.pyplot as plt
import sys
import numpy as np

import datetime

rhiTargets={
    'Suchet':{
        'az':311.8,
        'xlim':[0,15],
        'ar':1.2,
        'ytitle':1.1,
        'vn':12,
    },
    'Les Clees':{
        'az':279.8,
        'ar':2,
        'ytitle':1.1,
        'vn':40,
    },
    'Mont Tendre':{
        'az':235.8,
        'xlim':[0,15],
        'ar':1.2,
        'ytitle':1.1,
        'vn':12,
    },
}

sectorScansConfigs={
    'elev_low':{
        'el':5,
        'xlim':[-27,0],
    },
    'elev_high':{
        'el':8,
        'xlim':[-27,0],
    },
}

def plot_RHI(header,records,config,outname):
    
    #default configuration
    pltConfig={ 
        'target':'unknown',
        'xlim':[-15,15],
        'ylim':[0,10],
        'ar':2,
        'ytitle':1.05,
        'vn':40
    }
    
    pyart_instance = process_dataset('RHI',header,records['RHI'],config)
    display = pyart.graph.RadarDisplay(pyart_instance)  
    az = pyart_instance.azimuth['data'].data[0]

    #set the specific configuration in function of the azimuth
    for target in rhiTargets:
        if abs(rhiTargets[target]['az']-az)<2:
            pltConfig.update(rhiTargets[target])
            pltConfig['target']=target
    
    azname = str(round(az))
        
    fig = plt.figure(figsize=[15, 3])
    ax = fig.add_subplot(131)
    var = 'Zh'
    display.plot(var, cmap='jet')
    display.set_limits(xlim=pltConfig['xlim'],ylim=pltConfig['ylim'])
    display.set_aspect_ratio(pltConfig['ar'])
        
    ax = fig.add_subplot(132)
    var = '-Zdr'
    pyart_instance.add_field('-Zdr',pyart_instance.fields['Zdr'])
    pyart_instance.fields['-Zdr']['data'].data[:] = -pyart_instance.fields['Zdr']['data'].data[:]
    display.plot(var,cmap='jet',vmin=-1,vmax=3)
    display.set_limits(xlim=pltConfig['xlim'],ylim=pltConfig['ylim'])
    display.set_aspect_ratio(pltConfig['ar'])
        
    ax = fig.add_subplot(133)
    var = 'RVel'
    display.plot(var,cmap='bwr',vmin=-pltConfig['vn'],vmax=pltConfig['vn'])
    display.set_aspect_ratio(pltConfig['ar'])
    display.set_limits(xlim=pltConfig['xlim'],ylim=pltConfig['ylim'])
        
    fig.suptitle(pltConfig['target'],x=.5,y=pltConfig['ytitle'],fontsize=14,fontweight='bold')
    fig.savefig(outname+'_az'+azname,bbox_inches='tight')#,dpi=200)
    plt.close()

def plot_sector(header,records,config,outname):
    pyart_instance = process_dataset('SECTOR_SCAN',header,records['Point'],config)  
    el = round(pyart_instance.elevation['data'][-20:].mean())
    
    pltConfig={
        'xlim':[-27,27],
        'ylim':[-27,27],
    }
    plotGraph=False
    
    for config in sectorScansConfigs:
        if abs(sectorScansConfigs[config]['el']-el)<=1:
            pltConfig.update(sectorScansConfigs[config])
            plotGraph=True
    if plotGraph:
        
        dt = datetime.datetime.utcfromtimestamp(pyart_instance.time['data'][0])
        dtstr = dt.strftime('%Y-%m-%dT%H:%M:%SZ') 
        
        x = np.array(pyart_instance.gate_x['data'])/1000
        y = np.array(pyart_instance.gate_y['data'])/1000
        pyart_instance.add_field('-Zdr',pyart_instance.fields['Zdr'])
        pyart_instance.fields['-Zdr']['data'].data[:] = -pyart_instance.fields['Zdr']['data'].data[:]
        elevations = pyart_instance.elevation['data']
        plots=[
        {
            'title':'Reflectivity',
            'cbar_label':'Reflectivity (dBZ)',
            'var':'Zh',
            'subplot':131,
            'cmap':'jet',
            'vmin':0,
            'vmax':55,
        },
        {
            'title':'Diff. reflectivity',
            'cbar_label':'Diff. reflectivity (dBZ)',
            'var':'-Zdr',
            'subplot':132,
            'cmap':'jet',
            'vmin':-1,
            'vmax':3,
        },
        {
            'title':'Mean Doppler velocity',
            'cbar_label':'Mean Doppler velocity (m/s)',
            'var':'RVel',
            'subplot':133,
            'cmap':'bwr',
            'vmin':-40,
            'vmax':40,
        },
        ]
        fig = plt.figure(figsize=[9, 3])
        for subplotCfg in plots:
            Var = pyart_instance.fields[subplotCfg['var']]['data']
            Var[abs(elevations-pltConfig['el'])>.2,:] = np.nan
            ax = fig.add_subplot(subplotCfg['subplot'])
            ax.set_aspect('equal')
            im=ax.pcolormesh(x,y,Var,cmap='jet',vmin=subplotCfg['vmin'],vmax=subplotCfg['vmax'])
            ax.set_xlim(pltConfig['xlim'])
            ax.set_ylim(pltConfig['ylim'])
            ax.set_title(subplotCfg['title'])
            ax.set_xlabel('Range x (km)',fontsize=10)
            ax.set_ylabel('Range y (km)',fontsize=10)
            ax.yaxis.set_label_coords(-0.3, 0.5)
            cbar=fig.colorbar(im,ax=ax)
            cbar.set_label(subplotCfg['cbar_label'],fontsize=10)
            cbar.ax.tick_params(labelsize=8)
        
        fig.suptitle('Elevation %d - '%el + dtstr,x=.5,y=1.05,fontsize=14,fontweight='bold')
        fig.savefig(outname+'_el'+str(pltConfig['el']),bbox_inches='tight')
        plt.close()


