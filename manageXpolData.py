import os
import subprocess
import re
import shutil
import psutil
import glob
import sys
#from plotting.plot_RHI import plot_RHI
from envsensor import fileLogger

log_sync=fileLogger.fileLogger('/users/radarop/lteradar1/log/','syncXpolData')
log_rm=fileLogger.fileLogger('/users/radarop/lteradar1/log/','rmXpolData')


def syncXpolData(source_dir,dst_dir_List):
    syncs={}
    for dest_dir in dst_dir_List:
        syncs[dest_dir]=subprocess.Popen(["./syncData",source_dir,dest_dir], stdout=subprocess.PIPE)
        
    for dest_dir in dst_dir_List:
        syncs[dest_dir].wait()   
        logOut=syncs[dest_dir].stdout.read().decode()
        print({dest_dir:logOut})
        log_sync.insert({dest_dir:logOut})

"""
        
def checkUsedSpace(source_dir):
    check_used = subprocess.run(["df", "--output=pcent", source_dir],capture_output=True,text=True)
    used = int(re.findall('\d+',check_used.stdout))[0]
    return used"""

def rmXpolData(source_dir,thres):
    while True:
        try:
            diskUsage = psutil.disk_usage(source_dir)[3]
            print("call diskUsage "+str(diskUsage))
            if diskUsage > thres:
                files = glob.glob(source_dir+'/xpold/*/*.dat') #list files according to modification date
                files.sort(key = os.path.getmtime)
                file_to_rm = files[0] #select oldest file
                name = file_to_rm.split('/')[-1]
                date = file_to_rm.split('/')[-2]
                # remove file
                print("Removing"+file_to_rm)
                rmdata = subprocess.run(["rm","-v",file_to_rm],stdout=subprocess.PIPE)
                rmOut = rmdata.stdout.decode()

                fig_to_rm = source_dir+'/quicklooks/'+date+'/'+name[:-4]+'.png' #select corresponding quicklook
                # if there is a corresponding quicklook, delete it
                try:
                    rm_fig = subprocess.run(["rm","-v",fig_to_rm],stdout=subprocess.PIPE)
                    log_rm.insert({'file':file_to_rm,'rmOut':rmOut,'diskUsage':diskUsage,'rmquicklook':rm_fig.stdout.decode()})
                except:
                    log_rm.insert({'file':file_to_rm,'rmOut':rmOut,'diskUsage':diskUsage,'rmquicklook':'no quicklook'})
                    pass
                
            else:
                log_rm.insert({'diskUsage':diskUsage})
                break
                    
        except Exception as e:
            log_rm.insert({'error':e.message})
            break

            
            # TO DO: vérifier la correspondance des noms. Arborescence dans quicklooks aussi.


"""        
if __name__ == "__main__":
    source_dir = sys.argv[1]
    data_dir1 = sys.argv[2]
    data_dir2 = sys.argv[3]
    remote_dir = sys.argv[4]
    rm_thres = sys.argv[5]
    syncXpolData(source_dir,data_dir1,data_dir2,remote_dir)
    rmXpolData(source_dir,rm_thres)
"""
