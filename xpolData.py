import tornado.ioloop
import tornado.web
import tornado.autoreload
import time

import time
from datetime import datetime
import math
import threading
from envsensor import fileLogger

import json
import os

#TO DO: CHECK THIS
#dest_dir1a='/data1a'
#dest_dir1b='/data1b'
#remote_dir=''
xpol_data_source='/users/radarop/lteradar1'
rm_memory_thres=80
dest_dir = ['/data1a','/data1b','lteuser@ltesrv2.epfl.ch:/ltedata/PAPET_2020/']#'pi@enacpltepc25.epfl.ch:/data/']#,'
def intervalSleep(interval):
    now=time.time()
    time.sleep(int(now/interval+1)*interval-now)
        
    
def generateQuicklooks_loop():
    from plotting.generateQuicklooks import generateQuicklooks
    
    while True:
        try:
            # TO DO: CHECK THIS
            generateQuicklooks(xpol_data_source+'/xpold',xpol_data_source+'/quicklooks')
        except:
            pass
        intervalSleep(60)
        
        
def manageXpolData_loop():  
    from manageXpolData import syncXpolData, rmXpolData
    
    while True:
        try:
            syncXpolData(xpol_data_source,dest_dir)
            rmXpolData(xpol_data_source,rm_memory_thres)
        except Exception as e:
            print("error:"+e.message)
            pass
        intervalSleep(60)   
#from manageXpolData import syncXpolData, rmXpolData
#syncXpolData(xpol_data_source,dest_dir)

quicklooks_thread = threading.Thread(target=generateQuicklooks_loop)
manageXpolData_thread = threading.Thread(target=manageXpolData_loop)

quicklooks_thread.start()
manageXpolData_thread.start()

tornado.web.Application()
tornado.autoreload.start(check_time=1000)
tornado.ioloop.IOLoop.instance().start()

